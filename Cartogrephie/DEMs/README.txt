Les différents DEM à l'échelle du Mera :
- "Coregis" correspond au DEM pléiades utilisé par Fanny pour l'aminscissement. Il est disponnible dans l'éllipsoïde et le géoïde
- "Mera_Co30_DEM_UTM45" est le DEM Copernicus à la résolution de 30m
- "GDEM25" est le GDEM à la résolution de 25m, correspondant au DEM utilisé par Farinotti (les valeurs de surface sont disponnibles seulement sur le Mera découpé selon le RGI)
- "HIMALAYA_EASTNEPAL_GDEM" est le GDEM à la résolution de 50m, utilisé par Millan
