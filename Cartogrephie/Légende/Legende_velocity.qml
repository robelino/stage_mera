<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="1e+08" hasScaleBasedVisibilityFlag="0" maxScale="0" version="3.16.1-Hannover" styleCategories="AllStyleCategories">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <temporal enabled="0" mode="0" fetchMode="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <customproperties>
    <property key="WMSBackgroundLayer" value="false"/>
    <property key="WMSPublishDataSourceUrl" value="false"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="identify/format" value="Value"/>
  </customproperties>
  <pipe>
    <provider>
      <resampling zoomedOutResamplingMethod="bilinear" enabled="false" zoomedInResamplingMethod="bilinear" maxOversampling="2"/>
    </provider>
    <rasterrenderer classificationMax="70" opacity="1" alphaBand="-1" type="singlebandpseudocolor" band="1" classificationMin="0.0535382" nodataColor="">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
        <colorrampshader labelPrecision="0" classificationMode="2" clip="0" maximumValue="70" colorRampType="INTERPOLATED" minimumValue="0.0535382">
          <colorramp name="[source]" type="gradient">
            <prop k="color1" v="247,251,255,255"/>
            <prop k="color2" v="8,48,107,255"/>
            <prop k="discrete" v="0"/>
            <prop k="rampType" v="gradient"/>
            <prop k="stops" v="0.13;222,235,247,255:0.26;198,219,239,255:0.39;158,202,225,255:0.52;107,174,214,255:0.65;66,146,198,255:0.78;33,113,181,255:0.9;8,81,156,255"/>
          </colorramp>
          <item label="0" alpha="255" value="0.0535382" color="#f7fbff"/>
          <item label="7" alpha="255" value="7.04818438" color="#e4eff9"/>
          <item label="14" alpha="255" value="14.042830559999999" color="#d1e3f3"/>
          <item label="21" alpha="255" value="21.037476739999995" color="#bad6eb"/>
          <item label="28" alpha="255" value="28.032122919999996" color="#9ac8e1"/>
          <item label="35" alpha="255" value="35.026769099999996" color="#73b3d8"/>
          <item label="42" alpha="255" value="42.02141527999999" color="#529dcc"/>
          <item label="49" alpha="255" value="49.016061459999996" color="#3585c0"/>
          <item label="56" alpha="255" value="56.01070763999999" color="#1c6cb1"/>
          <item label="63" alpha="255" value="63.00535381999999" color="#08519c"/>
          <item label="70" alpha="255" value="70" color="#08306b"/>
        </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast gamma="1" contrast="0" brightness="0"/>
    <huesaturation colorizeRed="255" colorizeGreen="128" colorizeStrength="100" grayscaleMode="0" saturation="0" colorizeBlue="128" colorizeOn="0"/>
    <rasterresampler zoomedInResampler="bilinear" maxOversampling="2" zoomedOutResampler="bilinear"/>
    <resamplingStage>resamplingFilter</resamplingStage>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
