<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.16.1-Hannover" styleCategories="AllStyleCategories" minScale="1e+08" maxScale="0" hasScaleBasedVisibilityFlag="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <temporal fetchMode="0" enabled="0" mode="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <customproperties>
    <property key="WMSBackgroundLayer" value="false"/>
    <property key="WMSPublishDataSourceUrl" value="false"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="identify/format" value="Value"/>
  </customproperties>
  <pipe>
    <provider>
      <resampling zoomedOutResamplingMethod="nearestNeighbour" maxOversampling="2" enabled="false" zoomedInResamplingMethod="nearestNeighbour"/>
    </provider>
    <rasterrenderer alphaBand="-1" classificationMax="47.8417969" type="singlebandpseudocolor" opacity="1" classificationMin="-98.3632813" band="1" nodataColor="">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>MinMax</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
        <colorrampshader clip="0" minimumValue="-98.3632813" colorRampType="INTERPOLATED" labelPrecision="4" classificationMode="1" maximumValue="47.8417969">
          <colorramp type="gradient" name="[source]">
            <prop k="color1" v="68,1,84,255"/>
            <prop k="color2" v="253,231,37,255"/>
            <prop k="discrete" v="0"/>
            <prop k="rampType" v="gradient"/>
            <prop k="stops" v="0.0196078;70,8,92,255:0.0392157;71,16,99,255:0.0588235;72,23,105,255:0.0784314;72,29,111,255:0.0980392;72,36,117,255:0.117647;71,42,122,255:0.137255;70,48,126,255:0.156863;69,55,129,255:0.176471;67,61,132,255:0.196078;65,66,135,255:0.215686;63,72,137,255:0.235294;61,78,138,255:0.254902;58,83,139,255:0.27451;56,89,140,255:0.294118;53,94,141,255:0.313725;51,99,141,255:0.333333;49,104,142,255:0.352941;46,109,142,255:0.372549;44,113,142,255:0.392157;42,118,142,255:0.411765;41,123,142,255:0.431373;39,128,142,255:0.45098;37,132,142,255:0.470588;35,137,142,255:0.490196;33,142,141,255:0.509804;32,146,140,255:0.529412;31,151,139,255:0.54902;30,156,137,255:0.568627;31,161,136,255:0.588235;33,165,133,255:0.607843;36,170,131,255:0.627451;40,174,128,255:0.647059;46,179,124,255:0.666667;53,183,121,255:0.686275;61,188,116,255:0.705882;70,192,111,255:0.72549;80,196,106,255:0.745098;90,200,100,255:0.764706;101,203,94,255:0.784314;112,207,87,255:0.803922;124,210,80,255:0.823529;137,213,72,255:0.843137;149,216,64,255:0.862745;162,218,55,255:0.882353;176,221,47,255:0.901961;189,223,38,255:0.921569;202,225,31,255:0.941176;216,226,25,255:0.960784;229,228,25,255:0.980392;241,229,29,255"/>
          </colorramp>
          <item alpha="255" label="-98,3633" value="-98.3632813" color="#440154"/>
          <item alpha="255" label="-95,4965" value="-95.49652136767004" color="#46085c"/>
          <item alpha="255" label="-92,6297" value="-92.62974681483226" color="#471063"/>
          <item alpha="255" label="-89,7630" value="-89.7629868825023" color="#481769"/>
          <item alpha="255" label="-86,8962" value="-86.89621232966452" color="#481d6f"/>
          <item alpha="255" label="-84,0294" value="-84.02945239733455" color="#482475"/>
          <item alpha="255" label="-81,1627" value="-81.1626924650046" color="#472a7a"/>
          <item alpha="255" label="-78,2959" value="-78.295903291659" color="#46307e"/>
          <item alpha="255" label="-75,4291" value="-75.4291141183134" color="#453781"/>
          <item alpha="255" label="-72,5623" value="-72.5623249449678" color="#433d84"/>
          <item alpha="255" label="-69,6957" value="-69.6956819767004" color="#414287"/>
          <item alpha="255" label="-66,8289" value="-66.82889280335479" color="#3f4889"/>
          <item alpha="255" label="-63,9621" value="-63.962103630009196" color="#3d4e8a"/>
          <item alpha="255" label="-61,0953" value="-61.095314456663594" color="#3a538b"/>
          <item alpha="255" label="-58,2285" value="-58.228525283318" color="#38598c"/>
          <item alpha="255" label="-55,3617" value="-55.3617361099724" color="#355e8d"/>
          <item alpha="255" label="-52,4951" value="-52.495093141705" color="#33638d"/>
          <item alpha="255" label="-49,6283" value="-49.6283039683594" color="#31688e"/>
          <item alpha="255" label="-46,7615" value="-46.761514795013795" color="#2e6d8e"/>
          <item alpha="255" label="-43,8947" value="-43.894725621668194" color="#2c718e"/>
          <item alpha="255" label="-41,0279" value="-41.0279364483226" color="#2a768e"/>
          <item alpha="255" label="-38,1611" value="-38.161147274977" color="#297b8e"/>
          <item alpha="255" label="-35,2944" value="-35.294358101631396" color="#27808e"/>
          <item alpha="255" label="-32,4277" value="-32.427715133364" color="#25848e"/>
          <item alpha="255" label="-29,5609" value="-29.560925960018395" color="#23898e"/>
          <item alpha="255" label="-26,6941" value="-26.694136786672786" color="#218e8d"/>
          <item alpha="255" label="-23,8273" value="-23.82734761332719" color="#20928c"/>
          <item alpha="255" label="-20,9606" value="-20.960558439981597" color="#1f978b"/>
          <item alpha="255" label="-18,0938" value="-18.093769266636002" color="#1e9c89"/>
          <item alpha="255" label="-15,2271" value="-15.227126298368603" color="#1fa188"/>
          <item alpha="255" label="-12,3603" value="-12.360337125023008" color="#21a585"/>
          <item alpha="255" label="-9,4935" value="-9.493547951677385" color="#24aa83"/>
          <item alpha="255" label="-6,6268" value="-6.626758778331805" color="#28ae80"/>
          <item alpha="255" label="-3,7600" value="-3.759969604986182" color="#2eb37c"/>
          <item alpha="255" label="-0,8932" value="-0.893180431640587" color="#35b779"/>
          <item alpha="255" label="1,9736" value="1.973608741705007" color="#3dbc74"/>
          <item alpha="255" label="4,8403" value="4.840251709972407" color="#46c06f"/>
          <item alpha="255" label="7,7070" value="7.707040883318001" color="#50c46a"/>
          <item alpha="255" label="10,5738" value="10.57383005666361" color="#5ac864"/>
          <item alpha="255" label="13,4406" value="13.440619230009204" color="#65cb5e"/>
          <item alpha="255" label="16,3074" value="16.3074084033548" color="#70cf57"/>
          <item alpha="255" label="19,1742" value="19.174197576700408" color="#7cd250"/>
          <item alpha="255" label="22,0408" value="22.040840544967793" color="#89d548"/>
          <item alpha="255" label="24,9076" value="24.907629718313416" color="#95d840"/>
          <item alpha="255" label="27,7744" value="27.774418891658996" color="#a2da37"/>
          <item alpha="255" label="30,6412" value="30.64120806500462" color="#b0dd2f"/>
          <item alpha="255" label="33,5080" value="33.5079972383502" color="#bddf26"/>
          <item alpha="255" label="36,3748" value="36.37478641169581" color="#cae11f"/>
          <item alpha="255" label="39,2414" value="39.24142937996321" color="#d8e219"/>
          <item alpha="255" label="42,1082" value="42.10821855330879" color="#e5e419"/>
          <item alpha="255" label="44,9750" value="44.975007726654425" color="#f1e51d"/>
          <item alpha="255" label="47,8418" value="47.841796900000006" color="#fde725"/>
        </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast brightness="0" contrast="0" gamma="1"/>
    <huesaturation colorizeBlue="128" colorizeGreen="128" grayscaleMode="0" colorizeOn="0" colorizeRed="255" saturation="0" colorizeStrength="100"/>
    <rasterresampler maxOversampling="2"/>
    <resamplingStage>resamplingFilter</resamplingStage>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
