<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis hasScaleBasedVisibilityFlag="0" minScale="1e+08" version="3.16.1-Hannover" styleCategories="AllStyleCategories" maxScale="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <temporal fetchMode="0" mode="0" enabled="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <customproperties>
    <property key="WMSBackgroundLayer" value="false"/>
    <property key="WMSPublishDataSourceUrl" value="false"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="identify/format" value="Value"/>
  </customproperties>
  <pipe>
    <provider>
      <resampling zoomedInResamplingMethod="nearestNeighbour" zoomedOutResamplingMethod="nearestNeighbour" maxOversampling="2" enabled="false"/>
    </provider>
    <rasterrenderer nodataColor="" type="singlebandpseudocolor" classificationMax="91.9257813" alphaBand="-1" opacity="1" band="1" classificationMin="-66.9013672">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>MinMax</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
        <colorrampshader colorRampType="INTERPOLATED" minimumValue="-66.9013672" clip="0" maximumValue="91.9257813" labelPrecision="0" classificationMode="1">
          <colorramp type="gradient" name="[source]">
            <prop v="202,0,32,255" k="color1"/>
            <prop v="5,113,176,255" k="color2"/>
            <prop v="0" k="discrete"/>
            <prop v="gradient" k="rampType"/>
            <prop v="0.25;244,165,130,255:0.5;247,247,247,255:0.75;146,197,222,255" k="stops"/>
          </colorramp>
          <item color="#ca0020" alpha="255" value="-66.9013672" label="-67"/>
          <item color="#e05653" alpha="255" value="-46.253837895" label="-46"/>
          <item color="#f5a887" alpha="255" value="-25.606308589999998" label="-26"/>
          <item color="#f6d3c4" alpha="255" value="-4.9587792849999985" label="-5"/>
          <item color="#eff3f5" alpha="255" value="15.68875002" label="16"/>
          <item color="#bbd9e8" alpha="255" value="36.33627932500001" label="36"/>
          <item color="#81bbd9" alpha="255" value="56.98380863" label="57"/>
          <item color="#3d93c3" alpha="255" value="76.04306645000001" label="76"/>
          <item color="#0571b0" alpha="255" value="91.9257813" label="92"/>
        </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast gamma="1" brightness="0" contrast="0"/>
    <huesaturation colorizeStrength="100" colorizeOn="0" saturation="0" grayscaleMode="0" colorizeRed="255" colorizeBlue="128" colorizeGreen="128"/>
    <rasterresampler maxOversampling="2"/>
    <resamplingStage>resamplingFilter</resamplingStage>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
