<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis styleCategories="AllStyleCategories" minScale="1e+08" maxScale="0" version="3.16.1-Hannover" hasScaleBasedVisibilityFlag="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <temporal mode="0" fetchMode="0" enabled="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <customproperties>
    <property key="WMSBackgroundLayer" value="false"/>
    <property key="WMSPublishDataSourceUrl" value="false"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="identify/format" value="Value"/>
  </customproperties>
  <pipe>
    <provider>
      <resampling maxOversampling="2" zoomedOutResamplingMethod="bilinear" zoomedInResamplingMethod="bilinear" enabled="false"/>
    </provider>
    <rasterrenderer type="singlebandpseudocolor" alphaBand="-1" band="1" nodataColor="" opacity="1" classificationMax="108" classificationMin="0">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
        <colorrampshader minimumValue="0" colorRampType="INTERPOLATED" clip="0" labelPrecision="0" maximumValue="108" classificationMode="2">
          <colorramp type="gradient" name="[source]">
            <prop v="247,251,255,255" k="color1"/>
            <prop v="8,48,107,255" k="color2"/>
            <prop v="0" k="discrete"/>
            <prop v="gradient" k="rampType"/>
            <prop v="0.13;222,235,247,255:0.26;198,219,239,255:0.39;158,202,225,255:0.52;107,174,214,255:0.65;66,146,198,255:0.78;33,113,181,255:0.9;8,81,156,255" k="stops"/>
          </colorramp>
          <item alpha="255" label="0" value="0" color="#f7fbff"/>
          <item alpha="255" label="4" value="4" color="#f0f7fd"/>
          <item alpha="255" label="8" value="8" color="#e9f2fb"/>
          <item alpha="255" label="12" value="12" color="#e2eef9"/>
          <item alpha="255" label="16" value="16" color="#dbe9f6"/>
          <item alpha="255" label="20" value="20" color="#d4e5f4"/>
          <item alpha="255" label="24" value="24" color="#cde0f2"/>
          <item alpha="255" label="28" value="28" color="#c6dbef"/>
          <item alpha="255" label="32" value="32" color="#bbd7ec"/>
          <item alpha="255" label="36" value="36" color="#b0d2e8"/>
          <item alpha="255" label="40" value="40" color="#a4cde4"/>
          <item alpha="255" label="44" value="44" color="#97c7e0"/>
          <item alpha="255" label="48" value="48" color="#89bfdd"/>
          <item alpha="255" label="52" value="52" color="#7ab7da"/>
          <item alpha="255" label="56" value="56" color="#6cafd6"/>
          <item alpha="255" label="60" value="60" color="#60a6d2"/>
          <item alpha="255" label="64" value="64" color="#549ecd"/>
          <item alpha="255" label="68" value="68" color="#4896c9"/>
          <item alpha="255" label="72" value="72" color="#3e8ec4"/>
          <item alpha="255" label="76" value="76" color="#3484bf"/>
          <item alpha="255" label="80" value="80" color="#2b7bba"/>
          <item alpha="255" label="84" value="84" color="#2172b6"/>
          <item alpha="255" label="88" value="88" color="#1968ae"/>
          <item alpha="255" label="92" value="92" color="#125ea6"/>
          <item alpha="255" label="96" value="96" color="#0a549e"/>
          <item alpha="255" label="100" value="100" color="#08488f"/>
          <item alpha="255" label="104" value="104" color="#083c7d"/>
          <item alpha="255" label="108" value="108" color="#08306b"/>
        </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast contrast="0" brightness="0" gamma="1"/>
    <huesaturation colorizeOn="0" colorizeBlue="128" colorizeStrength="100" colorizeRed="255" colorizeGreen="128" saturation="0" grayscaleMode="0"/>
    <rasterresampler maxOversampling="2" zoomedInResampler="bilinear" zoomedOutResampler="bilinear"/>
    <resamplingStage>resamplingFilter</resamplingStage>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
