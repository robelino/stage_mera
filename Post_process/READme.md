Ce répertoire contient le fichier "post_process.py" permettant de mener des opérations sur l'ensemble des données collectées. Les fichiers associés au code sont tous dans le répertoir tandis que les fichiers de résultats sont enregistrés dans le répertoir "OUTPUT".

# 1 : data radar 2021 post-processing
Cette section permet de mener des opérations sur les données radar collectées :

- analyse de la surface libre avec les DEM ;  
- calcul des différences d'épaisseurs avec les reconstructions Farinotti et Millan ;  
- recalibration de nouvelles cartes d'épaisseurs minimisant la RMSE ;  
- tracé de l'épaisseur en fonction de l'altitude ;  
- analyse du profil en long

# 2 : Stakes post processing
Cette section permet de comparer les différentes vitesses avec celle observées aux balises :

- calcul de la différence entre les normes des vitesses ;  
- calcul de l'angle avec les vitesses aux balises

# 3 : data radar 2009 post-processing
Cette section permet de mener des opérations sur les profils en travers obtenus en 2009 :

- calcul de l'épaisseur de glace et de la surface libre ;  
- comparaison du bed du glacier avec les reconstructions recalibrées Farinotti et Millan  ;
- tracé des profils en travers
