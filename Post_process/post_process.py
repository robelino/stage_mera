import pandas as pd
import numpy as np
import math
import xarray as xr
import matplotlib.pyplot as plt
import statistics
from mpl_toolkits import mplot3d


radar = pd.read_excel('post_process.xlsx')
DEM_F = xr.open_dataset('DEM_F.nc')
DEM_M = xr.open_dataset('DEM_M.nc')
Copernicus = xr.open_dataset('Copernicus.nc')
Coregis = xr.open_dataset('COREGIS.nc')
Coregis2 = xr.open_dataset('COREGIS_ellps.nc')
thick_F = xr.open_dataset('Farinotti_thick_Mera.nc')
thick_M = xr.open_dataset('Millan_thick_Mera.nc')

stakes = pd.read_excel('post_process.xlsx',sheet_name=5)
v_x_M = xr.open_dataset('VX_Mera.nc')
v_y_M = xr.open_dataset('VY_Mera.nc')

radar_line = pd.read_excel('post_process.xlsx',sheet_name=1)

radar_2009_z1 = pd.read_excel('post_process.xlsx',sheet_name=3)
radar_2009_z2 = pd.read_excel('post_process.xlsx',sheet_name=4)

x_F = thick_F.x.values
y_F = thick_F.y.values

x_M = thick_M.x.values
y_M = thick_M.y.values

#mask to include the map of Mera (section pplot)
h = np.ma.array(thick_F.Band1,mask=thick_F.Band1!=0)


#1 : data radar 2021 post-processing

X = radar.X.values
Y = radar.Y.values
l=len(X)

thick = radar.épaisseur.values

#1_1_1 : Surface analysis

#DEM_F
Interp = DEM_F.interp(x=(X),y=(Y),method="nearest")
DEM_F_interp = [Interp.Band1.values[i][i] for i in range(l)]
diff_DEM_F = radar.altitude - DEM_F_interp

#Compute the RMSE
square = np.square(diff_DEM_F)
MSE = square.mean()
RMSE_DEM_F = np.sqrt(MSE)

#DEM_M
Interp = DEM_M.interp(x=(X),y=(Y),method="nearest")
DEM_M_interp = [Interp.Band1.values[i][i] for i in range(l)]
diff_DEM_M = radar.altitude - DEM_M_interp

#Compute the RMSE
square = np.square(diff_DEM_M)
MSE = square.mean()
RMSE_DEM_M = np.sqrt(MSE)

#Copernicus
Interp = Copernicus.interp(x=(X),y=(Y),method="nearest")
Copernicus_interp = [Interp.Band1.values[i][i] for i in range(l)]
diff_Copernicus = radar.altitude - Copernicus_interp

#Compute the RMSE
square = np.square(diff_Copernicus)
MSE = square.mean()
RMSE_Copernicus = np.sqrt(MSE)

#Coregis
Interp = Coregis.interp(x=(X),y=(Y),method="nearest")
Coregis_interp = [Interp.Band1.values[i][i] for i in range(l)]
diff_Coregis = radar.altitude - Coregis_interp

#Compute the RMSE
square = np.square(diff_Coregis)
MSE = square.mean()
RMSE_Coregis = np.sqrt(MSE)



#1_2_1 : Compute the difference between radar and "Farinotti model 4" thickness
Interp = thick_F.interp(x=(X),y=(Y),method="nearest")
thick_interp = [Interp.Band1.values[i][i] for i in range(l)]
radar['thick_interp_F'] = thick_interp

diff_F_thick = thick - thick_interp
radar['diff_F_thick'] = diff_F_thick

#Compute the RMSE, IQR and median
square = np.square(diff_F_thick)
MSE = square.mean()
RMSE_F_thick = np.sqrt(MSE)

bias_F_thick = diff_F_thick.mean()

q3, q1 = np.percentile(diff_F_thick, [75 ,25])
IQR_F_thick = q3 - q1
median_F_thick = statistics.median(diff_F_thick)


#1_2_2 : Plot the colormap of thickness difference
plt.figure()
plt.imshow(h,extent=(x_F[0],x_F[-1],y_F[0],y_F[-1]),origin='lower',cmap='cool')

#include each points in the map
plt.title('Difference between radar and Farinotti model 4')
plt.xlabel('x')
plt.ylabel('y')
plt.scatter(X,Y,c=diff_F_thick,cmap="gist_rainbow")
plt.colorbar()
plt.text(486200,3068000,'RMSE= %s' % (str.format("{0:.3f}", RMSE_F_thick)),fontsize=10,bbox=dict(facecolor='red', alpha=0.5))
plt.show()
#plt.savefig('Difference_F_thick.png')


#1_2_3 : Recalibrating the data thickness of Farinotti 

#Define "alpha", the coefficient factor to minimize the difference between radar observation and Millan model 
alpha=(sum(2*radar.thick_interp_F*radar.épaisseur))/(sum(2*(radar.thick_interp_F)**2))
thick_F_new = alpha*thick_F.Band1.values

#Compute the RMSE, IQE and median
diff_F_rec_thick = thick - np.array(thick_interp)*alpha
square = np.square(diff_F_rec_thick)
MSE = square.mean()
RMSE_F_rec_thick = np.sqrt(MSE)

bias_F_rec_thick = diff_F_rec_thick.mean()

q3, q1 = np.percentile(diff_F_rec_thick, [75 ,25])
IQR_F_rec_thick = q3 - q1
median_F_rec_thick = statistics.median(diff_F_rec_thick)


#export the new model into netCDF
ds = xr.Dataset(
{"thick":(('y','x'),np.nan_to_num(thick_F_new,nan=0))},
coords={
"x":thick_F['x'],
"y":thick_F['y'],
},
)
#ds.to_netcdf('Farinotti_thick_recalibrate.nc')



#1_3_1 : Compute the difference between radar and "Millan model" thickness

Interp = thick_M.interp(x=(X),y=(Y),method="nearest")
thick_interp = [Interp.Band1.values[i][i] for i in range(l)]
radar['thick_interp_M'] = thick_interp

#We remove rows with nan values
radar = radar.dropna()
thick = radar.épaisseur.values
thick_interp = [x for x in thick_interp if not math.isnan(x)]

diff_M_thick = thick - thick_interp
radar['diff_M_thick'] = diff_M_thick


#Compute the RMSE, IQR and median
square = np.square(diff_M_thick)
MSE = square.mean()
RMSE_M_thick = np.sqrt(MSE)

bias_M_thick = diff_M_thick.mean()

q3, q1 = np.percentile(diff_M_thick, [75 ,25])
IQR_M_thick = q3 - q1
median_M_thick = statistics.median(diff_M_thick)

#Eventually we save a new xls file with the values of difference
#radar.to_excel('OUTPUT/Files/radar_postprocess.xlsx')


#1_3_2 : Plot the colormap of thickness difference

plt.figure()
plt.imshow(h,extent=(x_F[0],x_F[-1],y_F[0],y_F[-1]),origin='lower',cmap='cool')

plt.title('Difference between radar and Millan')
plt.xlabel('x')
plt.ylabel('y')
plt.scatter(X,Y,c=diff_M_thick,cmap="gist_rainbow")
plt.colorbar()
plt.text(486200,3068000,'RMSE= %s' % (str.format("{0:.3f}", RMSE_M_thick)),fontsize=10,bbox=dict(facecolor='red', alpha=0.5))
plt.show()
#plt.savefig('Difference_M_thick.png')


#boxplot for the two models

data = [diff_F_thick, diff_M_thick]
labels = ['Farinotti','Millan']

fig, ax = plt.subplots()
ax.boxplot(data,patch_artist = True,vert=0,whis=1.5,widths=.25,labels=labels)
plt.xlabel('Ice thickness difference (m)')
plt.title('Boxplot of ice thickness difference (whis=1.5IQR)')
plt.text(-20,1.2,'RMSE = %.2f' % (RMSE_F_thick),bbox=dict(facecolor='red', alpha=0.5))
plt.text(15,1.2,'Bias = %.2f' % (bias_F_thick),bbox=dict(facecolor='orange', alpha=0.5))
#plt.text(-20,0.75,'IQR = %.2f' % (IQR_F_thick),bbox=dict(facecolor='blue', alpha=0.5))
#plt.text(15,0.75,'Median = %.2f' % (median_F_thick),bbox=dict(facecolor='yellow', alpha=0.5))
plt.text(-30,2.2,'RMSE = %.2f' % (RMSE_M_thick),bbox=dict(facecolor='red', alpha=0.5))
plt.text(10,2.2,'Bias = %.2f' % (bias_M_thick),bbox=dict(facecolor='orange', alpha=0.5))
#plt.text(-30,1.75,'IQR = %.2f' % (IQR_M_thick),bbox=dict(facecolor='blue', alpha=0.5))
#plt.text(10,1.75,'Median = %.2f' % (median_M_thick),bbox=dict(facecolor='yellow', alpha=0.5))
plt.show()



#1_3_3 : Recalibrating the data thickness of Romain Millan

#Define "alpha", the coefficient factor to minimize the difference between radar observation and Millan model 
alpha=(sum(2*radar.thick_interp_M*radar.épaisseur))/(sum(2*(radar.thick_interp_M)**2))
thick_M_new = alpha*thick_M.Band1.values

#Compute the RMSE, IQE and median
diff_M_rec_thick = thick - np.array(thick_interp)*alpha
square = np.square(diff_M_rec_thick)
MSE = square.mean()
RMSE_M_rec_thick = np.sqrt(MSE)

bias_M_rec_thick = diff_M_rec_thick.mean()

q3, q1 = np.percentile(diff_M_rec_thick, [75 ,25])
IQR_M_rec_thick = q3 - q1
median_M_rec_thick = statistics.median(diff_M_rec_thick)

#export the new model into netCDF
ds = xr.Dataset(
{"thick":(('y','x'),np.nan_to_num(thick_M_new,nan=0))},
coords={
"x":thick_M['x'],
"y":thick_M['y'],
},
)
#ds.to_netcdf('Millan_thick_recalibrate.nc')

#1_4 : Plot the ice thickness depending of the elevation
z = radar_line.altitude.values
thick = radar_line.épaisseur.values

radar_trace = pd.read_excel('post_process.xlsx',sheet_name=2)
data = [radar_trace.T1.dropna(), radar_trace.T2.dropna(), radar_trace.T3.dropna(), radar_trace.T4.dropna(), radar_trace.T5.dropna(), radar_trace.T6.dropna(), radar_trace.T7.dropna()]
labels = ['5368-5413', '5417-5478', '5492-5562', '5568-5778', '5793-5959', '6070-6162', '6183-6272']

plt.scatter(z,thick,cmap="gist_rainbow")
plt.xlabel('elevation (m)')
plt.ylabel('ice thickness (m)')
plt.show()
#plt.savefig('thickness(z)')

#boxplot for the two models

data = [diff_F_thick, diff_F_rec_thick, diff_M_thick, diff_M_rec_thick]
labels = ['Farinotti', 'Farinotti recalibrate', 'Millan', 'Millan recalibrate']

fig, ax = plt.subplots()
ax.boxplot(data,patch_artist = True,vert=0,whis=1.5,widths=.25,labels=labels)
plt.xlabel('Ice thickness difference (m)')
plt.title('Boxplot of ice thickness difference (whis=1.5IQR)')
plt.text(-58,1.3,'RMSE = %.2f    Bias = %.2f    IQR = %.2f    Median = %.2f' % (RMSE_F_thick, bias_F_thick, IQR_F_thick, median_F_thick),bbox=dict(facecolor='red', alpha=0.3))
plt.text(-58,2.3,'RMSE = %.2f    Bias = %.2f    IQR = %.2f    Median = %.2f' % (RMSE_F_rec_thick, bias_F_rec_thick,IQR_F_rec_thick, median_F_rec_thick),bbox=dict(facecolor='blue', alpha=0.3))
plt.text(-58,3.3,'RMSE = %.2f    Bias = %.2f    IQR = %.2f    Median = %.2f' % (RMSE_M_thick, bias_M_thick, IQR_M_thick, median_M_thick),bbox=dict(facecolor='red', alpha=0.3))
plt.text(-58,4.3,'RMSE = %.2f    Bias = %.2f    IQR = %.2f    Median = %.2f' % (RMSE_M_rec_thick, bias_M_rec_thick, IQR_M_rec_thick, median_M_rec_thick),bbox=dict(facecolor='blue', alpha=0.3))
plt.show()


#1_5 : Longitudinal profile

thick_F_new = xr.open_dataset('Farinotti_thick_recalibrate.nc')
thick_M_new = xr.open_dataset('Millan_thick_recalibrate.nc')

X = radar_line.X.values
Y = radar_line.Y.values
l=len(X)

#1_5_1 : Longitudinal profile of surface elevation

#DEM_F
Interp = DEM_F.interp(x=(X),y=(Y),method="linear")
DEM_F_interp = [Interp.Band1.values[i][i] for i in range(l)]

#DEM_M
Interp = DEM_M.interp(x=(X),y=(Y),method="linear")
DEM_M_interp = [Interp.Band1.values[i][i] for i in range(l)]

#Copernicus
Interp = Copernicus.interp(x=(X),y=(Y),method="linear")
DEM_Cop_interp = [Interp.Band1.values[i][i] for i in range(l)]

#Coregis
Interp = Coregis.interp(x=(X),y=(Y),method="linear")
DEM_Cor_interp = [Interp.Band1.values[i][i] for i in range(l)]

#1_5_2 : Farinotti

Interp = thick_F_new.interp(x=(X),y=(Y),method="linear")
thick_interp_F = [Interp.thick.values[i][i] for i in range(l)]
bed_F = np.array(DEM_Cor_interp) - np.array(thick_interp_F)


#1_5_3 : Millan

Interp = thick_M_new.interp(x=(X),y=(Y),method="linear")
thick_interp_M = [Interp.thick.values[i][i] for i in range(l)]
bed_M = np.array(DEM_Cor_interp) - np.array(thick_interp_M)

#Eventually we save a new xls file with the values of difference
#radar_line.to_excel('OUTPUT/Files/radar_line_postprocess.xlsx')


#1_5_4 : Plot profiles
#zone 1
plt.figure()
plt.plot(radar_line.distance,z)
plt.plot(radar_line.distance,radar_line.bed, color='black')
plt.plot(radar_line.distance,bed_F, color='yellow')
plt.plot(radar_line.distance,bed_M, color='red')
plt.legend(['surface','bed','bed F rec','bed M rec'], fontsize=14)
plt.xlabel('distance (m)', fontsize=15)
plt.ylabel('elevation (m)', fontsize=15)
plt.xticks(fontsize = 15)
plt.yticks(fontsize = 15)
plt.xlim((0,1000))
plt.ylim((5320,5420))
plt.show()

#zone 2
plt.figure()
plt.plot(radar_line.distance,z)
plt.plot(radar_line.distance,radar_line.bed, color='black')
plt.plot(radar_line.distance,bed_F, color='yellow')
plt.plot(radar_line.distance,bed_M, color='red')
plt.legend(['surface','bed','bed F rec','bed M rec'], fontsize=14)
plt.xlabel('distance (m)', fontsize=15)
plt.ylabel('elevation (m)', fontsize=15)
plt.xticks(fontsize = 15)
plt.yticks(fontsize = 15)
plt.xlim((1000,3500))
plt.ylim((5420,6100))
plt.show()

#zone 3
plt.figure()
plt.plot(radar_line.distance,z)
plt.plot(radar_line.distance,radar_line.bed, color='black')
plt.plot(radar_line.distance,bed_F, color='yellow')
plt.plot(radar_line.distance,bed_M, color='red')
plt.legend(['surface','bed','bed F rec','bed M rec'], fontsize=14)
plt.xlabel('distance (m)', fontsize=15)
plt.ylabel('elevation (m)', fontsize=15)
plt.xticks(fontsize = 15)
plt.yticks(fontsize = 15)
plt.xlim((3500,3800))
plt.ylim((6000,6300))
plt.show()



# 2 : Stakes post processing

X = stakes.x_init
Y = stakes.y_init
l=len(X)


v_x = stakes.velocity_x
v_y = stakes.velocity_y
v = np.transpose([v_x,v_y])
v_norm = stakes.velocity.values

#2_1_1 : Compute the difference between velocity from stakes and velocity compute from Farinotti

data_F = xr.open_dataset('grid_F_merged.nc')

Interp = data_F.interp(x=(X),y=(Y),method="nearest")
v_x_interp = [Interp.xvelsurf.values[0][i][i] for i in range(l)]
v_y_interp = [Interp.yvelsurf.values[0][i][i] for i in range(l)]
stakes['v_x_F_interp'] = v_x_interp
stakes['v_y_F_interp'] = v_y_interp

norme_interp = np.sqrt(stakes.v_x_F_interp**2+stakes.v_y_F_interp**2)
stakes['||v_F||_interp'] = norme_interp

diff_F_velocity = v_norm - norme_interp
stakes['diff_F_velocity'] = diff_F_velocity

v_interp = np.transpose((v_x_interp , v_y_interp))

angle_F = np.zeros(l)
for i in range(l):
    angle_F[i] = np.arccos(np.dot(v[i], v_interp[i]) / (np.linalg.norm(v[i]) * np.linalg.norm(v_interp[i])))*(180/math.pi)
stakes['angle_F'] = np.around(angle_F, decimals=1)

#Compute the RMSE, bias, IQE and median
square = np.square(diff_F_velocity)
MSE = square.mean()
RMSE_F_velocity = np.sqrt(MSE)

bias_F_velocity = diff_F_velocity.mean()

q3, q1 = np.percentile(diff_F_velocity, [75 ,25])
IQR_F_velocity = q3 - q1
median_F_velocity = statistics.median(stakes.diff_F_velocity.values)


square = np.square(angle_F)
MSE = square.mean()
RMSE_F_angle = np.sqrt(MSE)

bias_F_angle = angle_F.mean()

q3, q1 = np.percentile(angle_F, [75 ,25])
IQR_F_angle = q3 - q1
median_F_angle = statistics.median(angle_F)


#2_1_2 : Export the norm of velocity of Farinotti for mapping

v_F = np.sqrt(data_F.xvelsurf**2+data_F.yvelsurf**2)

ds = xr.Dataset(
{"V":(('y','x'),np.nan_to_num(v_F[0],nan=0)),
 "V_x":(('y','x'),np.nan_to_num(data_F.xvelsurf[0],nan=0)),
 "V_y":(('y','x'),np.nan_to_num(data_F.yvelsurf[0],nan=0))},
coords={
"x":data_F['x'],
"y":data_F['y'],
},
)
ds.to_netcdf('V_F.nc')

#Run this part when the calibration will be better

#2_2_1 : Compute the difference between velocity from stakes and velocity compute from Farinotti recalibrate

data_F_recalibrate = xr.open_dataset('grid_F_recalibrate.nc')

Interp = data_F_recalibrate.interp(x=(X),y=(Y),method="nearest")
v_x_interp = [Interp.xvelsurf.values[0][i][i] for i in range(l)]
v_y_interp = [Interp.yvelsurf.values[0][i][i] for i in range(l)]
stakes['v_x_F_rec_interp'] = v_x_interp
stakes['v_y_F_rec_interp'] = v_y_interp

norme_interp = np.sqrt(stakes.v_x_F_rec_interp**2+stakes.v_y_F_rec_interp**2)
stakes['||v_F_rec||_interp'] = norme_interp

diff_F_rec_velocity = v_norm - norme_interp
stakes['diff_F_rec_velocity'] = diff_F_rec_velocity

v_interp = np.transpose((v_x_interp , v_y_interp))

angle_F_rec=np.zeros(l)
for i in range(l):
    angle_F_rec[i] = np.arccos(np.dot(v[i], v_interp[i]) / (np.linalg.norm(v[i]) * np.linalg.norm(v_interp[i])))*(180/math.pi)
stakes['angle_F_rec'] = np.around(angle_F_rec, decimals=1)

#Compute the RMSE, bias, IQE and median
square = np.square(diff_F_rec_velocity)
MSE = square.mean()
RMSE_F_rec_velocity = np.sqrt(MSE)

bias_F_rec_velocity = diff_F_rec_velocity.mean()

q3, q1 = np.percentile(stakes.diff_F_rec_velocity.values, [75 ,25])
IQR_F_rec_velocity = q3 - q1
median_F_rec_velocity = statistics.median(stakes.diff_F_rec_velocity.values)


square = np.square(angle_F_rec)
MSE = square.mean()
RMSE_F_rec_angle = np.sqrt(MSE)

bias_F_rec_angle = angle_F_rec.mean()

q3, q1 = np.percentile(angle_F_rec, [75 ,25])
IQR_F_rec_angle = q3 - q1
median_F_rec_angle = statistics.median(angle_F_rec)


#2_2_2 : Export the norm of velocity of Farinotti recalibrate for mapping

v_F_rec = np.sqrt(data_F_recalibrate.xvelsurf**2+data_F_recalibrate.yvelsurf**2)

ds = xr.Dataset(
{"V":(('y','x'),np.nan_to_num(v_F_rec[0],nan=0)),
 "V_x":(('y','x'),np.nan_to_num(data_F_recalibrate.xvelsurf[0],nan=0)),
 "V_y":(('y','x'),np.nan_to_num(data_F_recalibrate.yvelsurf[0],nan=0))},
coords={
"x":data_F_recalibrate['x'],
"y":data_F_recalibrate['y'],
},
)
ds.to_netcdf('V_F_rec.nc')


#2_3_1 : Compute the difference between velocity from stakes and Millan model

Interp = v_x_M.interp(x=(X),y=(Y),method="nearest")
v_x_interp = [Interp.Band1.values[i][i] for i in range(l)]
stakes['v_x_M_interp'] = v_x_interp

Interp = v_y_M.interp(x=(X),y=(Y),method="nearest")
v_y_interp = [Interp.Band1.values[i][i] for i in range(l)]
stakes['v_y_M_interp'] = v_y_interp

norme_interp = np.sqrt(stakes.v_x_M_interp**2+stakes.v_y_M_interp**2)
stakes['||v_M||_interp'] = norme_interp

diff_M_velocity = v_norm - norme_interp
stakes['diff_M_velocity'] = diff_M_velocity

v_interp = np.transpose((v_x_interp , v_y_interp))

angle_M=np.zeros(l)
for i in range(l):
    angle_M[i] = np.arccos(np.dot(v[i], v_interp[i]) / (np.linalg.norm(v[i]) * np.linalg.norm(v_interp[i])))*(180/math.pi)
stakes['angle_M'] = np.around(angle_M, decimals=1)


#Compute the RMSE, IQE and median
square = np.square(diff_M_velocity)
MSE = square.mean()
RMSE_M_velocity = np.sqrt(MSE)

bias_M_velocity = diff_M_velocity.mean()

q3, q1 = np.percentile(diff_M_velocity, [75 ,25])
IQR_M_velocity = q3 - q1
median_M_velocity = statistics.median(diff_M_velocity)


square = np.square(angle_M)
MSE = square.mean()
RMSE_M_angle = np.sqrt(MSE)

bias_M_angle = angle_M.mean()

q3, q1 = np.percentile(angle_M, [75 ,25])
IQR_M_angle = q3 - q1
median_M_angle = statistics.median(angle_M)

#2_3_2 : Export the norm of velocity of Millan for mapping

v_M = np.sqrt(v_x_M.Band1**2+v_y_M.Band1**2)

ds = xr.Dataset(
{"V":(('y','x'),np.nan_to_num(v_M,nan=0)),
 "V_x":(('y','x'),np.nan_to_num(v_x_M.Band1,nan=0)),
 "V_y":(('y','x'),np.nan_to_num(v_y_M.Band1,nan=0))},
coords={
"x":v_x_M['x'],
"y":v_x_M['y'],
},
)
ds.to_netcdf('V_M.nc')

#2_4 : boxplots

data = [diff_F_velocity, diff_F_rec_velocity, diff_M_velocity]
labels = ['Farinotti', 'Farinotti recalibrate', 'Millan']

fig, ax = plt.subplots()
ax.boxplot(data,patch_artist = True,vert=0,whis=1.5,widths=.25,labels=labels)
plt.xlabel('Velocity difference (m/year)')
plt.title('Boxplot of velocity difference (whis=1.5IQR)')
plt.text(-2.5,1.3,'RMSE = %.2f' % (RMSE_F_velocity),bbox=dict(facecolor='red', alpha=0.5))
plt.text(2,1.3,'Bias = %.2f' % (bias_F_velocity),bbox=dict(facecolor='orange', alpha=0.5))
#plt.text(-2.5,0.65,'IQR = %.2f' % (IQR_F_velocity),bbox=dict(facecolor='blue', alpha=0.5))
#plt.text(2,0.65,'Median = %.2f' % (median_F_velocity),bbox=dict(facecolor='yellow', alpha=0.5))
plt.text(-1,2.3,'RMSE = %.2f' % (RMSE_F_rec_velocity),bbox=dict(facecolor='red', alpha=0.5))
plt.text(3.5,2.3,'Bias = %.2f' % (bias_F_rec_velocity),bbox=dict(facecolor='orange', alpha=0.5))
#plt.text(-1,1.65,'IQR = %.2f' % (IQR_F_rec_velocity),bbox=dict(facecolor='blue', alpha=0.5))
#plt.text(3,1.65,'Median = %.2f' % (median_F_rec_velocity),bbox=dict(facecolor='yellow', alpha=0.5))
plt.text(-3.5,3.3,'RMSE = %.2f' % (RMSE_M_velocity),bbox=dict(facecolor='red', alpha=0.5))
plt.text(1,3.3,'Bias = %.2f' % (bias_M_velocity),bbox=dict(facecolor='orange', alpha=0.5))
#plt.text(-3.5,2.65,'IQR = %.2f' % (IQR_M_velocity),bbox=dict(facecolor='blue', alpha=0.5))
#plt.text(1,2.65,'Median = %.2f' % (median_M_velocity),bbox=dict(facecolor='yellow', alpha=0.5))
plt.show()

data = [angle_F, angle_F_rec, angle_M]
labels = ['Farinotti', 'Farinotti recalibrate', 'Millan']

fig, ax = plt.subplots()
ax.boxplot(data,patch_artist = True,vert=0,whis=1.5,widths=.25,labels=labels)
plt.xlabel('Angle (°)')
plt.title('Boxplot of angle difference (whis=1.5IQR)')
#plt.text(0,1.3,'RMSE = %.2f' % (RMSE_F_angle),bbox=dict(facecolor='red', alpha=0.5))
plt.text(50,1.3,'Mean = %.2f' % (bias_F_angle),bbox=dict(facecolor='orange', alpha=0.5))
#plt.text(0,0.65,'IQR = %.2f' % (IQR_F_angle),bbox=dict(facecolor='blue', alpha=0.5))
#plt.text(50,0.65,'Median = %.2f' % (median_F_angle),bbox=dict(facecolor='yellow', alpha=0.5))
#plt.text(0,2.3,'RMSE = %.2f' % (RMSE_F_rec_angle),bbox=dict(facecolor='red', alpha=0.5))
plt.text(50,2.3,'Mean = %.2f' % (bias_F_rec_angle),bbox=dict(facecolor='orange', alpha=0.5))
#plt.text(0,1.65,'IQR = %.2f' % (IQR_F_rec_angle),bbox=dict(facecolor='blue', alpha=0.5))
#plt.text(50,1.65,'Median = %.2f' % (median_F_rec_angle),bbox=dict(facecolor='yellow', alpha=0.5))
#plt.text(40,3.3,'RMSE = %.2f' % (RMSE_M_angle),bbox=dict(facecolor='red', alpha=0.5))
plt.text(100,3.3,'Mean = %.2f' % (bias_M_angle),bbox=dict(facecolor='orange', alpha=0.5))
#plt.text(40,2.65,'IQR = %.2f' % (IQR_M_angle),bbox=dict(facecolor='blue', alpha=0.5))
#plt.text(100,2.65,'Median = %.2f' % (median_M_angle),bbox=dict(facecolor='yellow', alpha=0.5))
plt.show()

#Eventually we save a new xls file with the values of difference
#stakes.to_excel('OUTPUT/Files/stakes_postprocess.xlsx')

#3 : data radar 2009 post-processing

X = radar_2009_z1.X.values
Y = radar_2009_z1.Y.values
l=len(X)

bed = radar_2009_z1.Zbed

#3_1_1 : Define the surface and ice thickness from COREGIS at z = 5 350 m

#surface
Interp = Coregis2.interp(x=(X),y=(Y),method="linear")
surface_interp = [Interp.Band1.values[i][i] for i in range(l)]
radar_2009_z1['surface'] = surface_interp

thick_z1 = surface_interp - bed
radar_2009_z1['thick'] = thick_z1

#3_1_2 : Define the bed by Farinotti model
Interp = thick_F_new.interp(x=(X),y=(Y),method="linear")
thick_F_z1 = [Interp.thick.values[i][i] for i in range(l)]

bed_F_z1 = np.array(surface_interp) - np.array(thick_F_z1)
radar_2009_z1['bed_F'] = bed_F_z1

#3_1_3 : Define the bed by Millan model
Interp = thick_M_new.interp(x=(X),y=(Y),method="linear")
thick_M_z1 = [Interp.thick.values[i][i] for i in range(l)]

bed_M_z1 = np.array(surface_interp) - np.array(thick_M_z1)
radar_2009_z1['bed_M'] = bed_M_z1

#3_1_4 : RMSE

diff_bed_F = bed - bed_F_z1
square = np.square(diff_bed_F)
MSE = square.mean()
RMSE_F_z1 = np.sqrt(MSE)


diff_bed_M = bed - bed_M_z1
square = np.square(diff_bed_M)
MSE = square.mean()
RMSE_M_z1 = np.sqrt(MSE)

#Eventually we save a new xls file with the values of difference
#radar_2009_z1.to_excel('OUTPUT/Files/radar_2009_z1_postprocess.xlsx')


#3_2_1 : Define the surface and ice thickness from COREGIS at z = 5 540 m

X = radar_2009_z2.X
Y = radar_2009_z2.Y
l=len(X)

bed = radar_2009_z2.Zbed

Interp = Coregis2.interp(x=(X),y=(Y),method="linear")
surface_interp = [Interp.Band1.values[i][i] for i in range(l)]
radar_2009_z2['surface'] = surface_interp

thick_z2 = surface_interp - bed
radar_2009_z2['thick'] = thick_z2

#3_2_2 : Define the bed by Farinotti model
Interp = thick_F_new.interp(x=(X),y=(Y),method="linear")
thick_F_z2 = [Interp.thick.values[i][i] for i in range(l)]

bed_F_z2 = np.array(surface_interp) - np.array(thick_F_z2)
radar_2009_z2['bed_F'] = bed_F_z2

#3_2_3 : Define the bed by Millan model
Interp = thick_M_new.interp(x=(X),y=(Y),method="linear")
thick_M_z2 = [Interp.thick.values[i][i] for i in range(l)]

bed_M_z2 = np.array(surface_interp) - np.array(thick_M_z2)
radar_2009_z2['bed_M'] = bed_M_z2

#3_2_4 : RMSE

diff_bed_F = bed - bed_F_z2
square = np.square(diff_bed_F)
MSE = square.mean()
RMSE_F_z2 = np.sqrt(MSE)


diff_bed_M = bed - bed_M_z2
square = np.square(diff_bed_M)
MSE = square.mean()
RMSE_M_z2 = np.sqrt(MSE)


#Eventually we save a new xls file with the values of difference
#radar_2009_z2.to_excel('OUTPUT/Files/radar_2009_z2_postprocess.xlsx')

#3_3 : Plot the profiles

radar_2009_z1.dropna()
radar_2009_z2.dropna()

#Z=5350
plt.figure()
plt.plot(radar_2009_z1.Dist, radar_2009_z1.Zbed, color='black')
plt.plot(radar_2009_z1.Dist, radar_2009_z1.surface)
plt.plot(radar_2009_z1.Dist, radar_2009_z1.bed_F, color='yellow')
plt.plot(radar_2009_z1.Dist, radar_2009_z1.bed_M, color='red')
plt.title('Bed at z = 5 350 m')
plt.legend(['bed', 'surface', 'bed F rec', 'bed M rec'], fontsize=14)
plt.xlabel('distance (m)', fontsize=15)
plt.ylabel('elevation (m)', fontsize=15)
plt.xticks(fontsize = 15)
plt.yticks(fontsize = 15)
plt.show()
#Z=5540
plt.figure()
plt.plot(radar_2009_z2.Dist, radar_2009_z2.Zbed, color='black')
plt.plot(radar_2009_z2.Dist, radar_2009_z2.surface)
plt.plot(radar_2009_z2.Dist, radar_2009_z2.bed_F, color='yellow')
plt.plot(radar_2009_z2.Dist, radar_2009_z2.bed_M, color='red')
plt.title('Bed at z = 5 540 m')
plt.legend(['bed', 'surface', 'bed F rec', 'bed M rec'], fontsize=14)
plt.xlabel('distance (m)', fontsize=15)
plt.ylabel('elevation (m)', fontsize=15)
plt.xticks(fontsize = 15)
plt.yticks(fontsize = 15)
plt.show()
