# Modélisation de l'écoulement du glacier du Mera (février 2022 - avril 2022)

Ce répertoir fait l'objet d'un stage de 11 semaines à l'IGE avec l'objectif de modéliser l'écoulement du glacier du Mera avec le code Elmer Ice.

Voici la liste des répertoirs :

**Cartographie** : Tous les outils nécessaires à la visualisation des travaux effectués

**Documentation** : Précédents travaux de recherches qui ont été utilisés dans la présente étude

**Pictures** : Figures utilisés dans le rapport et la présentation

**Post process** : Code python permettant l'intercomparaison des différentes données

**Simulation** : Fichiers des différentes simulations effectuées
