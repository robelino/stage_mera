import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt

data = xr.open_dataset('Grid.nc')
dh = xr.open_dataset('../../data/dh_Mera.nc')
mask = xr.open_dataset('../../data/mask.nc')
data_results = pd.read_excel('../../data/Results.xlsx')

surface = data.orog.where(mask.Band1>0)
dh_simu = surface[4] - surface[0]
dV_simu = (dh_simu*50*50).sum()

dh = dh.Band1.where((mask.Band1>0) & (dh.Band1>-15))
dV = (dh*50*50).sum()

diff_dh = dh - dh_simu
RMSE = np.sqrt((np.square(diff_dh)).mean())

Bias = diff_dh.mean()

diff_dV = np.abs(dV-dV_simu)
error = (diff_dV / np.abs(dV))*100

ds = xr.Dataset(
{"diff_dh":(('y','x'),diff_dh.values),
 "dh_simu":(('y','x'),dh_simu.values)},
coords={
"x":data['x'],
"y":data['y'],
},
)

ds.to_netcdf('diff_dh.nc')

data_results['accu=accu abbla=Mean'] = [RMSE.values, Bias.values, dV.values, dV_simu.values, diff_dV.values, error.values]

data_results.to_excel('../../data/Results.xlsx')

plt.figure()
plt.scatter(dh_simu.values,surface[0].values)
plt.savefig('z(dh).png')
