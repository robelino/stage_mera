Tous les fichiers "Results" contiennent le calcul de process automatisés dans les simulations avec les fichiers "post_process.py" qui se trouvent dans les fichiers de simulation.  

Pour les calculs de bilan de masse, les résultats sont dans le répertoire et le post processing n'est pas automatisé dans la simulation. En plus des résultats Excel sauvegardés, des fichiers netCDF avec la carte des variations d'épaisseur ont été créés.
