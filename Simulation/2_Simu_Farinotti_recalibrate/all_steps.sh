#=========================================
# AUTHOR:   O. ROBELIN
# ORGANIZATION: IGE(IRD-France)
#
# VERSION: V1 
# CREATED: 2022-03
# MODIFIED: 
#  
#
#========================================== 

############################
#Header for functions calculating max & min of netcdf + directories variables
function ncmin { ncap2 -O -C -v -s "foo=${1}.min();print(foo)" ${2} ~/foo.nc | cut -f 3- -d ' ' ; }
function ncmax { ncap2 -O -C -v -s "foo=${1}.max();print(foo)" ${2} ~/foo.nc | cut -f 3- -d ' ' ; }

HOME_DIR=$(pwd)
data_path=$HOME_DIR/data
aster_data=$data_path/Mera_Co30_DEM_UTM45.tif
glacier_name=Mera
#############################

cd $HOME_DIR

############################
#create the folders for the results
mkdir wdirs/$glacier_name
mkdir wdirs/$glacier_name/INPUT
mkdir wdirs/$glacier_name/OUTPUT


#########1_DEM_process#########

cd $data_path

gdal_translate -a_srs EPSG:32645 Farinotti_thick_recalibrate.nc ../wdirs/$glacier_name/INPUT/thick.nc


cd $HOME_DIR/wdirs/$glacier_name
	
#########SURFACE DATA IMPORT
#we then warp the ASTERv3 surface dataset on the thickness raster
cs=25 #this is the resolution of the thickness data
xmin0=$(ncmin x INPUT/thick.nc)
xmax0=$(ncmax x INPUT/thick.nc)
ymin0=$(ncmin y INPUT/thick.nc)
ymax0=$(ncmax y INPUT/thick.nc)

xmin=$(echo "scale=10; $xmin0-$cs/2" | bc -l)
ymin=$(echo "scale=10; $ymin0-$cs/2" | bc -l)
xmax=$(echo "scale=10; $xmax0+$cs/2" | bc -l)
ymax=$(echo "scale=10; $ymax0+$cs/2" | bc -l)

echo $xmin $xmax $ymin $ymax
	
gdalwarp -ot Float32 -s_srs EPSG:32645 -t_srs EPSG:32645 -r bilinear -of GTiff -srcnodata -9999 -tr $cs $cs -te $xmin $ymin $xmax $ymax -co COMPRESS=DEFLATE -co PREDICTOR=1 -co ZLEVEL=6 -wo OPTIMIZE_SIZE=TRUE $aster_data INPUT/surface.tif
gdal_translate -of netcdf -a_srs EPSG:32645 INPUT/surface.tif INPUT/surface.nc


#########FINAL DEM PROCESSING
#eventually we merge the files into one contaning surface&bedrock DEMs.
ncrename -v Band1,surface INPUT/surface.nc
ncatted -a _FillValue,thick,d,, INPUT/thick.nc
ncks -A INPUT/thick.nc INPUT/surface.nc
ncap2 -s "bed=surface-thick" INPUT/surface.nc INPUT/DEMs.nc


#########2_meshing#########


#we first get the extent of the raster files
xmin=$(ncmin x INPUT/DEMs.nc)
xmax=$(ncmax x INPUT/DEMs.nc)
ymin=$(ncmin y INPUT/DEMs.nc)
ymax=$(ncmax y INPUT/DEMs.nc)

res=100.0 #this is the wanted resolution of the mesh (to be 

echo $xmin $xmax $ymin $ymax


cd INPUT

#We paste the reference rectangle.geo into the glacier directory
cp $HOME_DIR/elmer/rectangle.geo rectangle.geo


#gmsh converts the .geo file into a .msh
gmsh -1 -2 rectangle.geo -setnumber xmin $xmin -setnumber xmax $xmax \
	         -setnumber ymin $ymin -setnumber ymax $ymax \
			 -setnumber lc $res 


#Finally we cal ElmerGrid to convert the .msh into a readable file for Elmer
ElmerGrid 14 2 rectangle.msh -autoclean
ElmerGrid 2 5 rectangle


cd $HOME_DIR/wdirs/$glacier_name

#########3_initialisation#########

cp $HOME_DIR/elmer/INIT.sif INIT.sif
cp -RT $HOME_DIR/elmer/sif_extras .

##
ElmerSolver INIT.sif
##


#########4_diagnostic########
cp $HOME_DIR/elmer/steady.sif steady.sif

##
ElmerSolver steady.sif
##


#########5_rename########
cp $HOME_DIR/elmer/RenameVar.sh RenameVar.sh

##
./RenameVar.sh
##