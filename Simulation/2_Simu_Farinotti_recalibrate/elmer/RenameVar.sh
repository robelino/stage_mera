#!/bin/bash
FILE=Grid.nc

function EditNC {
  if [ ${name_in// } != ${name_out// } ]
  then
    ncrename -h -v ''"$name_in"'',$name_out $FILE
  fi	
  ncatted -h -a standard_name,$name_out,c,c,$std $FILE
  ncatted -h -a units,$name_out,c,c,''"$unit"'' $FILE
}

name_in=thickness
name_out=lithk
std="land_ice_thickness" 
unit="m"
EditNC

name_in="top zs"
name_out=orog
std="surface_altitude"
unit="m"
EditNC


name_in="top velocity 1"
name_out=xvelsurf
std="land_ice_surface_x_velocity"
unit="m a-1"
EditNC

name_in="top velocity 2"
name_out=yvelsurf
std="land_ice_surface_y_velocity"
unit="m a-1"
EditNC

name_in="top velocity 3"
name_out=zvelsurf
std="land_ice_surface_z_velocity"
unit="m a-1"
EditNC

name_in="velocity 1"
name_out=xvelbase
std="land_ice_basal_x_velocity"
unit="m a-1"
EditNC

name_in="velocity 2"
name_out=yvelbase
std="land_ice_basal_y_velocity"
unit="m a-1"
EditNC


name_in="velocity 3"
name_out=zvelbase
std="land_ice_basal_upward_velocity"
unit="m a-1"
EditNC


name_in="drag"
name_out=strbasemag
std="land_ice_basal_drag"
unit="MPa"
EditNC

name_in="slc"
name_out="C"
std="basal_friction_coefficient"
unit="MPa m-1 a"
EditNC

name_in="bed"
name_out="topg"
std="bedrock_altitude"
unit="m"
EditNC

