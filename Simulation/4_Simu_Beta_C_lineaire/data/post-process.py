import numpy as np
import pandas as pd
import math
import xarray as xr
import matplotlib.pyplot as plt
import statistics

C=0.01

stakes = pd.read_excel('../../../../data/Stakes.xlsx')
data = xr.open_dataset('grid.nc')
results = pd.read_excel('../../../../data/Results.xlsx')
data_results_vel = pd.read_excel('../../../../data/Results.xlsx', sheet_name=1)
data_results_beta = pd.read_excel('../../../../data/Results.xlsx', sheet_name=2)

# 1 : Velocity difference with stakes

X = stakes.x_init
Y = stakes.y_init
l=len(X)


v_x = stakes.velocity_x
v_y = stakes.velocity_y
v = np.transpose([v_x,v_y])
v_norm = stakes.velocity.values

#1_1 : Compute the velocity difference with stakes

Interp = data.interp(x=(X),y=(Y),method="nearest")
v_x_surf = np.array([Interp.xvelsurf.values[i][i] for i in range(l)])
v_y_surf = np.array([Interp.yvelsurf.values[i][i] for i in range(l)])

norm_surf = np.sqrt(v_x_surf**2+v_y_surf**2)

diff_velocity = v_norm - norm_surf
data_results_vel['C=%.2f'%C] = np.around(diff_velocity, decimals=1)

#1_2 : Compute the RMSE, bias, IQE and median
square = np.square(diff_velocity)
MSE = square.mean()
RMSE_velocity = np.sqrt(MSE)

bias_velocity = diff_velocity.mean()

q3, q1 = np.percentile(diff_velocity, [75 ,25])
IQR_velocity = q3 - q1
median_velocity = statistics.median(diff_velocity)

#compute beta
Interp = data.interp(x=(X),y=(Y),method="nearest")
v_x_base = np.array([Interp.xvelbase.values[i][i] for i in range(l)])
v_y_base = np.array([Interp.yvelbase.values[i][i] for i in range(l)])

norm_base = np.sqrt(v_x_base**2+v_y_base**2)
beta = norm_base / norm_surf
data_results_beta['C=%.2f'%C] = np.around(beta, decimals=2)
beta = beta.mean()

results['velocity(C=%.2f)'%C] = np.around([RMSE_velocity, bias_velocity, IQR_velocity, median_velocity, beta], decimals=2)
results['angle(C=%.2f)'%C] = np.around([RMSE_angle, bias_angle, IQR_angle, median_angle, np.nan], decimals=2)

#Eventually we save a new xls file with the values of difference
with pd.ExcelWriter('../../../../data/Results.xlsx') as writer:  

    results.to_excel(writer, sheet_name='Results')

    data_results_vel.to_excel(writer, sheet_name='Velocity')
    
    data_results_beta.to_excel(writer, sheet_name='Beta')
