#!/bin/bash
C=10
FILE=OUTPUT/RUN_C=${C}/Grid.nc

function EditNC {
  if [ ${name_in// } != ${name_out// } ]
  then
    ncrename -h -v ''"$name_in"'',$name_out $FILE
  fi	
  ncatted -h -a standard_name,$name_out,c,c,$std $FILE
  ncatted -h -a units,$name_out,c,c,''"$unit"'' $FILE
}

name_in="thickness"
name_out=lithk
std="land_ice_thickness" 
unit="m"
EditNC

name_in="top zs"
name_out=orog
std="surface_altitude"
unit="m"
EditNC


name_in="top velocity 1"
name_out=xvelsurf
std="land_ice_surface_x_velocity"
unit="m a-1"
EditNC

name_in="top velocity 2"
name_out=yvelsurf
std="land_ice_surface_y_velocity"
unit="m a-1"
EditNC

name_in="velocity 1"
name_out=xvelbase
std="land_ice_basal_x_velocity"
unit="m a-1"
EditNC

name_in="velocity 2"
name_out=yvelbase
std="land_ice_basal_y_velocity"
unit="m a-1"
EditNC


name_in="velocity 3"
name_out=zvelbase
std="land_ice_basal_upward_velocity"
unit="m a-1"
EditNC


name_in="bed"
name_out=topg
std="bedrock_altitude"
unit="m"
EditNC

name_in="int velocity 1"
name_out=xvelmean
std="land_ice_vertical_mean_x_velocity"
unit="m a-1"
EditNC

name_in="int velocity 2"
name_out=yvelmean
std="land_ice_vertical_mean_y_velocity"
unit="m a-1"
EditNC


name_in="uobs 1"
name_out=xvelobs
std="velocity_x_from_Millan_model"
unit="m a-1"
EditNC


name_in="uobs 2"
name_out=yvelobs
std="velocity_y_from_Millan_model"
unit="m a-1"
EditNC