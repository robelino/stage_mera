import pandas as pd
import matplotlib.pyplot as plt
import numpy as np



results = pd.read_excel('Results.xlsx')
velocity = pd.read_excel('Results.xlsx', sheet_name=1)
beta = pd.read_excel('Results.xlsx', sheet_name=2)

L = len(results.iloc[1,:]) - 2
l = len(velocity.values)

#data = (velocity.values)
data = [velocity.iloc[:,0], velocity.iloc[:,1], velocity.iloc[:,2], velocity.iloc[:,5]]
labels = [beta.columns[0],beta.columns[1], beta.columns[2],beta.columns[5]]

fig, ax = plt.subplots(figsize=(10,3))
ax.boxplot(data,patch_artist = True,vert=0,whis=1.5,widths=.25,labels=labels)
plt.xlabel('Velocity difference (m/year)')
plt.title('Boxplot of velocity difference (whis=1.5IQR)')


data = [beta.iloc[:,0], beta.iloc[:,1],beta.iloc[:,2],beta.iloc[:,5]]
labels = [beta.columns[0],beta.columns[1], beta.columns[2],beta.columns[5]]
colors = ['y', 'b', 'r','m']
plt.figure()
plt.hist(data, label=labels, color=colors)
plt.legend(prop ={'size': 10}, loc='lower right')
plt.xlabel('Beta values')
plt.show()
