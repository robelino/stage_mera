import numpy as np
import pandas as pd
import math
import xarray as xr
import matplotlib.pyplot as plt
import statistics

A=10

stakes = pd.read_excel('../../../../data/Stakes.xlsx')
data = xr.open_dataset('grid.nc')
results = pd.read_excel('../../../../data/Results.xlsx')
data_results = pd.read_excel('../../../../data/Results.xlsx', sheet_name=1)

# 1 : Velocity difference with stakes

X = stakes.x_init
Y = stakes.y_init
l=len(X)


v_x = stakes.velocity_x
v_y = stakes.velocity_y
v = np.transpose([v_x,v_y])
v_norm = stakes.velocity.values

#1_1 : Compute the velocity difference with stakes

Interp = data.interp(x=(X),y=(Y),method="nearest")
v_x_interp = np.array([Interp.xvelsurf.values[i][i] for i in range(l)])
v_y_interp = np.array([Interp.yvelsurf.values[i][i] for i in range(l)])

norme_interp = np.sqrt(v_x_interp**2+v_y_interp**2)

diff_velocity = v_norm - norme_interp
data_results['A=%f'%A] = diff_velocity

#1_2 : Compute the RMSE, bias, IQE and median
square = np.square(diff_velocity)
MSE = square.mean()
RMSE_velocity = np.sqrt(MSE)

bias_velocity = diff_velocity.mean()

q3, q1 = np.percentile(diff_velocity, [75 ,25])
IQR_velocity = q3 - q1
median_velocity = statistics.median(diff_velocity)


results['A=%f'%A] = [RMSE_velocity, bias_velocity, IQR_velocity, median_velocity]


#Eventually we save a new xls file with the values of difference
with pd.ExcelWriter('../../../../data/Results.xlsx') as writer:  

    results.to_excel(writer, sheet_name='Sheet_name_1')

    data_results.to_excel(writer, sheet_name='Sheet_name_2')