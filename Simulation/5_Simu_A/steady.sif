!---LUA BEGIN
! assert(loadfile('Parameters.lua'))()
! assert(loadfile('LuaFunctions.lua'))()
!---LUA END

! Local Parameters
#MESH="rectangle"
#name="1"

#Startyear = 0.0

#dt=0.5
#Ndt=1

#OutInter=1

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Header
  Mesh DB "INPUT/#MESH#" "."
  Results Directory "OUTPUT/RUN_A=#A#"
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Simulation
  Coordinate System  =  Cartesian 3D 

  Simulation Type = Steady

  Steady State Max Iterations = 1
  Steady State Min Iterations = 1

  !! Internal Extrusion
  include EXTRUDE.IN 

  Output File = "RUN_#name#_.result"
  Post File = "RUN_#name#_.vtu"
  vtu:VTU Time Collection = Logical True

  Restart File = "../RUN_0_.result "
  Restart Position = 0
  Restart Time = Real #Startyear
  Restart Before Initial Conditions = Logical True 

  max output level = 4
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Body 1
  Equation = 1
  Body Force = 1
  Material = 1
  Initial Condition = 1
End
Body 2
  Name= "surface"
  Equation = 2
  Material = 1
  Initial Condition = 2
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Initial Condition 1
  Top IcyMask = Real 1.0
End

Initial Condition 2   
  Zs = Equals surface
  IcyMask = Real 1.0
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Body Force 1
  Flow BodyForce 1 = Real 0.0    
  Flow BodyForce 2 = Real 0.0            
  Flow BodyForce 3 = Real #gravity 
End

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Material 1
! For the ice flow  
  Density = Real #rhoi   

  Viscosity Model = String "Glen"
  Viscosity = 1.0 ! Dummy but avoid warning output

  Glen Exponent = Real #n
  
  Critical Shear Rate = Real 1.0e-10
  Set Arrhenius Factor = Logical True
  Arrhenius Factor = Real #A

End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Map mesh to bottom and top surfaces
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Solver 1
  Equation = "MapCoordinate"
  Procedure = "StructuredMeshMapper" "StructuredMeshMapper"

! Extrusion direction
  Active Coordinate = Integer 3

  Displacement Mode = Logical False

! Check for critical thickness
  Correct Surface = Logical True
  Minimum Height = Real #MinH

! Top and bottom surfaces defined from variables
  Top Surface Variable Name = String "Zs"
  Bottom Surface Variable Name = String "bed"
End

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Compute Thickness and Depth
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Solver 2
  Equation = "HeightDepth 1"
  Procedure = "StructuredProjectToPlane" "StructuredProjectToPlane"

  Active Coordinate = Integer 3

  Project to everywhere = Logical True

  Operator 1 = Thickness
  Operator 2 = Depth
End

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  Icy Mask
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Solver 3
  ! to be executed on top surface (need Thickness)
  Equation = "IcyMask"
  Procedure = "../../elmer/IcyMaskSolver" "IcyMaskSolver"
  Variable = "IcyMask"
  Variable DOFs = 1
  
  Exported Variable 1 = Zs

 ! no matrix resolution
  Optimize Bandwidth = Logical False

  Toler = Real 1.0e-1
  Ice Free Thickness = Real #MinH
  Remove Isolated Points = Logical True
  Remove Isolated Edges = Logical True
End

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  Project Ice Mask in bulk
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Solver 4
  Equation = "HeightDepth 2"
  Procedure = "StructuredProjectToPlane" "StructuredProjectToPlane"
  Active Coordinate = Integer 3
  Variable 1 = IcyMask
  Operator 1 = top

  Project to everywhere = Logical True
End

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  Stokes 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Solver 5
  Equation = "Stokes-Vec"
  Procedure = "IncompressibleNSVec" "IncompressibleNSSolver"
  Stokes Flow = logical true

  Div-Curl Discretization = Logical False
  Stabilization Method = String Stabilized

  !linear settings:
  !------------------------------
  Linear System Solver = Iterative
  !Linear System Direct method = umfpack

  Linear System Iterative Method = GCR
  Linear System GCR Restart =  50

  Linear System Max Iterations  = 1000
  Linear System Preconditioning = ILU0
  Linear System Convergence Tolerance = 1.0e-08

  Linear System Residual Output = 10


  !Non-linear iteration settings:
  !------------------------------ 
  Nonlinear System Max Iterations = 50
  Nonlinear System Convergence Tolerance  = 1.0e-5
  Nonlinear System Newton After Iterations = 5
  Nonlinear System Newton After Tolerance = 1.0e-3
  Nonlinear System Reset Newton = Logical True

  ! Convergence on timelevel (not required here)
  !---------------------------------------------
  Steady State Convergence Tolerance = Real 1.0e-3

  Relative Integration Order = -1

  ! 1st iteration viscosity is constant
  Constant-Viscosity Start = Logical False


  Exported Variable 1 = -dofs 3 "top velocity"
  Exported Variable 2 = -dofs 3 "int velocity"
End

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  Project variables on the bed 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Solver 6
  Equation = "Proj"
  Procedure = "StructuredProjectToPlane" "StructuredProjectToPlane"
  Active Coordinate = Integer 3

  Project to everywhere = Logical True


 ! project surface velocity to the bottom layer => top velocity
 ! and compute the flux, int_bed^zs velocity dz => int velocity
  Variable 1 = "Velocity 1"
  Operator 1 = top
  Operator 2 = int

  Variable 3 = "Velocity 2"
  Operator 3 = top
  Operator 4 = int

 ! project zs to bottom layer
  Variable 5 = "Zs"
  Operator 5 = top

End

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Save on a regular netcdf grid
!  see "Saving data on uniform Cartesian grid" in the ElmerSolver Manual
! main adpatation is that it is saved in netcdf
! there is some small modifications what is now in the distrib.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Solver 7
  Equation = SaveGrid
  Procedure = "SaveGridData" "SaveGridData"
  Variable = -nooutput "savegrid_var"

  Filename Prefix  = File "OUTPUT/RUN_A=#A#/Grid"
 
 ! Results will be saved only on the bottom boundary where MaskBed=Logical True
  Mask Name = String "MaskBed"

  Check for Duplicates = Logical True

 ! We don't need a z-coordinate
  Suppress Extra Dimension = Logical True


  Single Precision = Logical True

 !Grid definition
  Grid dx = Real 50.0
  Grid Origin At Corner = Logical True
  Min Coordinate 1 = Real 486075.0
  Min Coordinate 2 = Real 3062575.0
  Max Coordinate 1 = Real 491675.0
  Max Coordinate 2 = Real 3068825.0

 !Variables to be saved
  Scalar Field 1 = String "bed"
  Scalar Field 2 = String "top Zs"
  Scalar Field 3 = String "thickness"
  Vector Field 1 = String "Velocity"
  Vector Field 2 = String "top Velocity"
  Vector Field 3 = String "int Velocity"
  Vector Field 4 = String "uobs"

  NetCDF Format = Logical True
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Equation 1
  Active Solvers(6) = 1 2 4 5 6 7
End

Equation 2
  Active Solvers(1) = 3
  Flow Solution Name = String "Flow Solution"
  !Convection = Computed
End

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Side
Boundary Condition 1
  Target Boundaries = 1
  Name = "side"

 ! pressure, gravity is negative
!  External Pressure = Variable Depth
!   REAL LUA "tx[0]*rhoi*gravity"

  Velocity 1 = Real 0.0
  Velocity 2 = Real 0.0

End

! Bedrock 
Boundary Condition 2
  Name = "bed"

  MaskBed=Logical True
 
  Normal-Tangential Velocity = Logical True
  Mass Consistent Normals = Logical True

  Velocity 1 = Real 0.0e0

  Slip Coefficient 2 = Real #C 
  Slip Coefficient 3 = Real #C 

!  Slip Coefficient 2 =  Variable Coordinate 1
!    Real Procedure "ElmerIceUSF" "Sliding_Weertman"
!  Slip Coefficient 3 =  Variable Coordinate 1
!    Real Procedure "ElmerIceUSF" "Sliding_Weertman"

!  Weertman Friction Coefficient = Real #C    
!  Weertman Exponent = Real $1.0/3.0
!  Weertman Linear Velocity = Real 0.1

 ! set no sliding if H< 2m
  Velocity 2 = Real 0.0e0
  Velocity 3 = Real 0.0e0
  Velocity 2 Condition = Variable "thickness"
    Real LUA "IfThenElse(tx[0]< 2.0, 1.0, -1.0)"
  Velocity 3 Condition = Variable "thickness"
    Real LUA "IfThenElse(tx[0]< 2.0, 1.0, -1.0)"
  
End

! Upper Surface
Boundary Condition 3
  Name = "upper surface"
  Body Id = 2
  
End

