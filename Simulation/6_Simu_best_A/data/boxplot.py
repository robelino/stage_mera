import pandas as pd
import matplotlib.pyplot as plt
import numpy as np



results = pd.read_excel('Results_1.xlsx')
data_results = pd.read_excel('Results_1.xlsx', sheet_name=1)

L = len(results.iloc[1,:])-2
l = len(data_results.values)

data = (data_results.values)
labels = list(data_results.columns)

fig, ax = plt.subplots(figsize=(10,5))
ax.boxplot(data,patch_artist = True,vert=0,whis=.25,widths=.25,labels=labels)
plt.xlabel('Velocity difference (m/year)')
plt.title('Boxplot of velocity difference (whis=0.25IQR)')
