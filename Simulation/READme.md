# Simulations

**1** : Simulation fusionnant toutes les données d'épaisseurs de la reconstruction Farinotti. Modèle simplifié avec un glissement nul et un facteur A=100

**2** : Simulation avec les mêmes paramètres mais en prenant la reconstruction d'épaisseurs de Farinotti recalibrée en entrée du Modèle

**3** : Simulation avec les mêmes paramètres mais en prenant la reconstruction d'épaisseurs de Millan recalibrée en entrée du Modèle

**4** : Simulations faisant verier le paramètre C pour trouver le Beta déterminé par Romain Millan. La première simulation utilise un coefficient de friction linéaire tandis que la seconde utilise un coefficient non linéaire. Les valeurs du coefficient C peuvent être modifiées dans le fichier "**data/C.txt**". Un fichier de post processing est intégré à la simulation, qui synthétisera les résultats dans le fichier "**data/Results**"

**5** : Simulation faisant verier le paramètre A en utilisant le paramètre C, pour une friction linéaire définit lors de la simulation précédente. Les valeurs du coefficient A peuvent être modifiées dans le fichier "**data/A.txt**". Un fichier de post processing est intégré à la simulation, qui synthétisera les résultats dans le fichier "**data/Results**". Une friction non-linéaire peut être implémentée, ce qui augmentera le temps de calcul mais n'affectera pas les résultats

**6** : Simulation avec la meilleure valeur du paramètre A, couplée au C déterminé précédemment. Un fichier de post processing est intégré à la simulation, qui synthétisera les résultats dans le fichier "**data/Results**"

**7** : Simulation transitoire intégrant le bilan de masse avec un gradient en zone d'accumulation et un autre en zone d'ablation. Les valeurs des gradients peuvent être changées dans le fichier "**Elmer/steady.sif**" dans le solveur "**SMB**". Le post processing se fait indépendemment de la simulation, dans la section "**Resultats**"

# DEM et épaisseurs

Pour chaque simulation, il suffit d'exécuter la commande "**./allsteps.sh**" dans le répertoire. Les fichiers de sortie seront créés dans le répertoire "**wdirs**".  
Il est possible de choisir le DEM utilisé (disponnibles dans "**Cartographie/DEMs**"), pour cela il suffit d'ajouter le format .tif dans le répertoire "**data**" et de mentionner son nom dans le fichier "**allsteps.sh**".  
Il est également possible de choisir le jeu d'épaisseurs en entrée. Il suffit d'ajouter le fichier au format netCDF dans le répertoire "**data**" et de mentionner son nom dans le fichier "**allsteps.sh**".  
